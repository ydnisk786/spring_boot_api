package com.exmple.airport;

import lombok.Value;

@Value
public class Coordinates {

    private double latitude, longitude;

}
