package com.example.oauth;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
 
@Configuration
public class AccessSecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		httpSecurity.authorizeRequests().antMatchers("/").permitAll();
	//	httpSecurity.authorizeRequests().antMatchers("/**").permitAll().and().csrf().disable();
		//httpSecurity.authorizeRequests().antMatchers("/").permitAll()
		//.antMatchers("/webjars/**").permitAll().antMatchers("/**").authenticated();
	
	}
	
}
