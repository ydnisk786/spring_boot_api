package com.example.service;

import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.exmple.airport.Location;
@Service
public class AirportService {

	@Autowired
	private RestTemplate restTemplate;
	
	@Value("${airportsurl}")
	private String airportsAPIUrl;
	@Value("${airportcode}")
	private String airportcodeAPIUrl;
	@Bean
	@Qualifier("oAuth2Rest")
	public ResponseEntity<String> retrieveAirportCode() throws Exception
	{
		try {
		// final String uri = "http://localhost:8080/airports";	
		   // RestTemplate restTemplate = new RestTemplate();
		 HttpHeaders headers=new HttpHeaders();
	        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	        HttpEntity<String> entity=new HttpEntity<String>(headers);
		  ResponseEntity<String> result = restTemplate.exchange(airportsAPIUrl,HttpMethod.POST,entity, String.class);
			 System.out.println("result--->"+result);
			 //return new ResponseEntity<>( HttpStatus.OK);
			// List<Location> LocationList = (List<Location>) result.getBody();
			 
			return result;
	}catch(Exception  e) {
	
		System.out.println("Something went wrong." +e);
	}
		return null;
	}
	
	@Bean
	public ResponseEntity<Location> airportCode() throws Exception
	{
		try {
		// final String uri = "http://localhost:8080/airports";	
		   // RestTemplate restTemplate = new RestTemplate();
		 HttpHeaders headers=new HttpHeaders();
	        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	        HttpEntity<String> entity=new HttpEntity<String>(headers);
		  ResponseEntity<Location> result = restTemplate.exchange(airportcodeAPIUrl,HttpMethod.POST,entity, Location.class);
			 System.out.println("result--->"+result);
			 //return new ResponseEntity<>( HttpStatus.OK);
			// List<Location> LocationList = (List<Location>) result.getBody();
			 
			return result;
	}catch(Exception  e) {
	
		System.out.println("Something went wrong." +e);
	}
		return null;
	}
}
